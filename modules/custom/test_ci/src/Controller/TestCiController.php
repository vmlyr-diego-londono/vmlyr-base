<?php

namespace Drupal\test_ci\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for test_ci routes.
 */
class TestCiController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
